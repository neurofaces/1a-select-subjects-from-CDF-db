function found=attributesInFilename(filename, attributes)

attr_names = fieldnames(attributes);
n_attr = numel(attr_names);

found = 1;

for na=1:n_attr
    
    filter = eval(['attributes.' attr_names{na}]);
    ntypes = numel(filter);
    
    for nf=1:ntypes
        
        if(~contains(filename, filter{nf}))
            found = 0;
        else
            found = 1;
            break;
        end
    end
    
    if(~found), return; end
end

if(found) disp(filename); end



% function found=attributesInFilename(filename, attributes)
% 
% disp(filename);
% 
% attr_names = fieldnames(attributes);
% n_attr = numel(attr_names);
% 
% for na=1:n_attr
%     
%     found = 0;
%     
%     filter = eval(['attributes.' attr_names{na}]);
%     ntypes = numel(filter);
%     
%     for nf=1:ntypes
%         
%         if(strfind(filename, filter{nf}))
%             found = 1;
%             break;
%         end
%     end
%     
%     if(~found), return; end
% end