function status = copyAllJpgFound(search_folder, target_folder, filter)

status = 0;

files = dir([search_folder{:} '\*.jpg']);
n_files = numel(files);

for nfl=1:n_files
    filename = fullfile(search_folder{:}, files(nfl).name);
    if(attributesInFilename(files(nfl).name, filter))
        copyfile(filename, fullfile(target_folder, files(nfl).name));
    end
end

status = 1;

% function found=attributesInFilename(filename, attributes)
% 
% disp(filename);
% 
% attr_names = fieldnames(attributes);
% n_attr = numel(attr_names);
% 
% for na=1:n_attr
%     
%     found = 0;
%     
%     filter = eval(['attributes.' attr_names{na}]);
%     ntypes = numel(filter);
%     
%     for nf=1:ntypes
%         
%         if(strfind(filename, filter{nf}))
%             found = 1;
%             break;
%         end
%     end
%     
%     if(~found), return; end
% end
