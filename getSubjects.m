function getSubjects(filter, target_path)

if(nargin < 2)
    disp('filter and target_parth must be provided')
    return;
end

% % % Configure the kind of subject wanted
% filter.gender = {'M'}; % options: M, F
% filter.race = {'A', 'B', 'L', 'W'}; % options: A, B, L, W
% filter.expression = {'N'}; % options: N (neutral), A (angry), F (fear), HC (happy, closed mouth), HO (happy, open mouth)
% 
% target_path = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\1-select-subjects-from-CDF-db\selected-faces';

if(~exist(target_path, 'dir'))
    mkdir(target_path);
end

% % Import names from csv
faces = importfile('E:\DOCTORADO\01_DATA\_NoGIT_common_dbs\CFD Version 2.0.3\CFD 2.0.3 Norming Data and Codebook.xlsx');
n_faces = numel(faces.Target);

% % Database folder
% db_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\DB_EXT\CFD Version 2.0\CFD 2.0 Images\';
db_folder = 'E:\DOCTORADO\01_DATA\_NoGIT_common_dbs\CFD Version 2.0.3\CFD 2.0.3 Images\';

% % Loop through all the folders inside the database and take the ones
% listed in the csv
h = waitbar(0,'Copying...');
for nf=1:n_faces
    copyAllJpgFound(fullfile(db_folder, faces.Target(nf)), target_path, filter);
    waitbar(nf/n_faces,h,'Copying...');
end
close(h);

% % Save all skill ratings
skill_names = {'Afraid', 'Angry', 'Attractive', 'Babyface', 'Disgusted', ...
        'Dominant', 'Feminine', 'Happy', 'Masculine', 'Prototypic', ...
        'Sad', 'Surprised', 'Threatening', 'Trustworthy', 'Unusual', ...
        'Age'};

fprintf('Saving skills...');
saveSkills(target_path, faces, skill_names, filter);
fprintf(' done!\n');
    


function saveSkills(target_path, faces, skill_names, filters)

facest = struct2table(faces);

files = dir([target_path '\*.jpg']);
files = strsplit([files(:).name], '.jpg')';
files(end) = [];

selected = zeros(numel(faces.Target), 1, 'uint8');
for nf=1:numel(files)
    file = strsplit(files{nf}, '-');
    file = strcat(file(2), '-', file(3));
    selected = selected + uint8(contains(faces.Target, file));
end

for ns=1:numel(skill_names)
    genders = strrep([filters.gender{:}], '-', '');
    ethnicities = strrep([filters.race{:}], '-', '');
    filename = fullfile(target_path, ['Rating_' genders '_' ethnicities '_' skill_names{ns} '.xlsx']);
    writetable(facest(selected==1, {'Target', skill_names{ns}}), filename);
end


